"use strict";
// ----=====Базовые типы, функции, литералы, обобщения, списки=====---- //
/**
 Задание 1:
  Напишите функцию которая принимает на вход год
 и возвращает век. Необходимо аннотировать все переменные.
 1705 => 18
 1900 => 19cls
 1601 => 17
 2000 => 20
 */
const getAge = (year) => Math.floor(year / 100) + 1;
// console.log(getAge(1990));
/**
 Задание 2:
  Напишите функцию которая принимает на вход строку
 и возвращает ее инвертированное представление. Необходимо
 аннотировать все переменные.
 'world' => 'dlrow'
 'word'  => 'drow'
 */
const stringReverse = (str) => str.split('').reverse().join('');
// console.log(stringReverse('dlrow'));
/**
 Задание 3:
  Напишите функцию которая принимает на вход массив
 чисел и возвращает сумму положительных элементов. Необходимо
 аннотировать все переменные.
 [1, -4, 7, 12] => 1 + 7 + 12 = 20
 */
const returnSumPositiveNumbers = (array) => array.reduce((pV, nV) => (nV > 0 ? pV + nV : pV));
function getVowelsLength(string) {
    const vowels = ['a', 'e', 'i', 'o', 'u'];
    return string.split('').filter((symbol) => vowels.includes(symbol.toLowerCase())).length;
}
var DnaEnum;
(function (DnaEnum) {
    DnaEnum["A"] = "T";
    DnaEnum["T"] = "A";
    DnaEnum["G"] = "C";
    DnaEnum["C"] = "G";
})(DnaEnum || (DnaEnum = {}));
const returnDna = (array) => [array, array.map((dnaItem) => DnaEnum[dnaItem])];
const Statuses = {
    Online: 'online',
    Offline: 'offline',
};
const availableFriends = (arr) => ({
    online: arr.filter((user) => user.status === Statuses.Online && user.lastActivity <= 10).map((user) => user.username),
    offline: arr.filter((user) => user.status === Statuses.Offline).map((user) => user.username),
    away: arr.filter((user) => user.status === Statuses.Online && user.lastActivity > 10).map((user) => user.username),
});
const getTuple = (array) => {
    let resultArr = [];
    for (let i = 0; i < array.length; i++) {
        if (resultArr.find((arr) => arr[0] === array[i])) {
            resultArr.map((arr) => arr[0] === array[i] && arr[1]++);
        }
        else {
            resultArr = [...resultArr, [array[i], 1]];
        }
    }
    return resultArr;
};
const user = JSON.parse('{"firstName": "Alisa", "age": 15}');
// obj is User2
const isUser = (obj) => {
    if (obj && typeof obj === 'object') {
        return 'firstName' in obj && 'age' in obj;
    }
    return false;
};
if (isUser(user)) {
    console.log(user.firstName, user.age);
}
const user31 = {
    firstName: "Lena",
    secondName: "Yagolovach"
};
const user41 = {
    id: `id1234`,
    addedAt: new Date(),
    updatedAt: new Date(),
    addedBy: `Uncle`,
    updatedBy: `Uncle`,
    firstName: `Some`,
    secondName: `Guy`,
    isAdmin: false,
    roles: [
        {
            id: `id0001`,
            addedAt: new Date(),
            updatedAt: new Date(),
            addedBy: `Uncle`,
            updatedBy: `Uncle`,
            name: `Uncle`
        }
    ]
};
class Figure {
    constructor(coordinateX, coordinateY) {
        this.coordinateX = coordinateX;
        this.coordinateY = coordinateY;
    }
    calcArea() {
        return 0;
    }
    calcPerimeter() {
        return 0;
    }
}
class Dot extends Figure {
    constructor(coordinateX, coordinateY) {
        super(coordinateX, coordinateY);
    }
}
class Circle extends Figure {
    constructor(coordinateX, coordinateY, radius) {
        super(coordinateX, coordinateY);
        this.radius = radius;
    }
    calcArea() {
        return Math.round(Math.PI * Math.pow(this.radius, 2));
    }
    calcPerimeter() {
        return Math.round(2 * this.radius * Math.PI);
    }
}
class Square extends Figure {
    constructor(coordinateX, coordinateY, sideA) {
        super(coordinateX, coordinateY);
        this.sideA = sideA;
    }
    calcArea() {
        return Math.pow(this.sideA, 2);
    }
    calcPerimeter() {
        return 4 * this.sideA;
    }
}
class Rectangle extends Figure {
    constructor(coordinateX, coordinateY, sideA, sideB) {
        super(coordinateX, coordinateY);
        this.sideA = sideA;
        this.sideB = sideB;
    }
    calcArea() {
        return this.sideA * this.sideB;
    }
    calcPerimeter() {
        return 2 * this.sideA + 2 * this.sideB;
    }
}
