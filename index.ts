// ----=====Базовые типы, функции, литералы, обобщения, списки=====---- //

/**
 Задание 1:
 Напишите функцию которая принимает на вход год
 и возвращает век. Необходимо аннотировать все переменные.
 1705 => 18
 1900 => 19cls
 1601 => 17
 2000 => 20
 */

const getAge = (year: number): number => Math.floor(year / 100) + 1;
// console.log(getAge(1990));


/**
 Задание 2:
 Напишите функцию которая принимает на вход строку
 и возвращает ее инвертированное представление. Необходимо
 аннотировать все переменные.
 'world' => 'dlrow'
 'word'  => 'drow'
 */

const stringReverse = (str: string): string => str.split('').reverse().join('');
// console.log(stringReverse('dlrow'));


/**
 Задание 3:
 Напишите функцию которая принимает на вход массив
 чисел и возвращает сумму положительных элементов. Необходимо
 аннотировать все переменные.
 [1, -4, 7, 12] => 1 + 7 + 12 = 20
 */

const returnSumPositiveNumbers = (array: number[]): number => array.reduce((pV: number, nV: number) => (nV > 0 ? pV + nV : pV));
// console.log(returnSumPositiveNumbers([1, -4, 7, 12]));


/**
 Задание 4:
 Напишите функцию которая принимает на вход строку
 и считает количество гласных в ней (a, e, i, o, u).
 'I like coding in TypeScript.' => 8
 */

type Vowels = 'a' | 'e' | 'i' | 'o' | 'u';

function getVowelsLength(string: string): number {
    const vowels: Vowels[] = ['a', 'e', 'i', 'o', 'u'];
    return string.split('').filter((symbol: string) => vowels.includes(symbol.toLowerCase() as Vowels)).length;
}

// console.log(getVowelsLength('I like coding in TypeScript.'));


/**
 Задание 5:
 Напишите функцию которая принимает на вход одну строну
 ДНК в виде массива строк, состоящих только из символов A,T,G,C и
 возвращает ее и дополняющую сторону в виде кортежа.
 В цепочках ДНК символы «А» и «Т» дополняют друг друга, как «С» и «G».
 Ваша функция получает одну сторону ДНК. Нить ДНК никогда не бывает
 пустой.
 Необходимо аннотировать все переменные.
 [A,T,G,C] => [ [A,T,G,C], [T,A,C,G] ]
 [G,T,A,T] => [ [G,T,A,T], [C,A,T,A] ]
 [A,A,A,A] => [ [A,A,A,A], [T,T,T,T] ]
 */

type Dna = 'A' | 'T' | 'G' | 'C';

enum DnaEnum {
    A = 'T',
    T = 'A',
    G = 'C',
    C = 'G'
}

const returnDna = (array: Dna[]): [Dna[], Dna[]] => [array, array.map((dnaItem: Dna) => DnaEnum[dnaItem])];
// console.log(returnDna([`A`,`T`,`G`,`C`]));
// console.log(returnDna([`G`, `T`, `A`, `T`]));
// console.log(returnDna([`A`,`A`,`A`,`A`]));


/**
 Задание 6:
 Напишите функцию которая принимает на вход массив пользователей
 с полями username, status, lastActivity и возвращает отчет по тому кто из
 них online, offline или ушел (away).
 У вас есть приложение для группового чата. Вы хотите показать своим пользователям,
 кто из их друзей онлайн и доступен для чата. Получив на вход массива объектов,
 содержащих имена пользователей, статус и время с момента последней активности
 (в минутах), создайте функцию для определения того, кто находится в сети,
 в автономном режиме и вне его. Если кто-то находится в сети, но его
 последняя активность была более 10 минут назад, он считается
 отсутствующим.

 Необходимо аннотировать все переменные.
 [{
 username: 'David',
 status: 'online',
 lastActivity: 10
 }, {
    username: 'Lucy',
    status: 'offline',
    lastActivity: 22
}, {
    username: 'Bob',
    status: 'online',
    lastActivity: 104
}] => {
    online: ['David'],
    offline: ['Lucy'],
    away: ['Bob']
}
 */

type Status = 'online' | 'offline' | 'away';

const Statuses: { [key: string]: Status } = {
    Online: 'online',
    Offline: 'offline',
};

interface User {
    username: string,
    status: Status,
    lastActivity: number
}

type Result = Record<Status, string[]>;

const availableFriends = (arr: User[]): Result => ({
    online: arr.filter((user: User) => user.status === Statuses.Online && user.lastActivity <= 10).map((user: User) => user.username),
    offline: arr.filter((user: User) => user.status === Statuses.Offline).map((user: User) => user.username),
    away: arr.filter((user: User) => user.status === Statuses.Online && user.lastActivity > 10).map((user: User) => user.username),
});

// console.log(availableFriends([{
//     username: 'David',
//     status: 'online',
//     lastActivity: 10
// }, {
//     username: 'Lucy',
//     status: 'offline',
//     lastActivity: 22
// }, {
//     username: 'Bob',
//     status: 'online',
//     lastActivity: 104
// }]));


/**
 Задание 7:
 Напишите функцию которая принимает на вход массив с примитивными
 типами данных и возвращает кортеж в котором показано сколько раз встречается
 каждое значение из входного массива.
 Необходимо аннотировать все переменные.
 ['Peter', 'Anna', 'Rose', 'Peter', 'Peter', 'Anna'] => [["Anna", 2], ["Peter", 3], ["Rose", 1]]
 [1, 10, 12, 2, 1, 10, 2, 2] => [[1, 2], [2, 3], [10, 2], [12, 1]]
 [true, false, true] => [[true, 2], [false, 1]]
 */

type Primitives = string | number | boolean;
type ResultArray = [Primitives, number];
type Report = Array<ResultArray>;

const getTuple = (array: Primitives[]): Report => {
    let resultArr: Report = [];
    for (let i = 0; i < array.length; i++) {
        if (resultArr.find((arr: ResultArray) => arr[0] === array[i])) {
            resultArr.map((arr: ResultArray) => arr[0] === array[i] && arr[1]++)
        } else {
            resultArr = [...resultArr, [array[i], 1]]
        }
    }
    return resultArr
}
// console.log(getTuple(['Peter', 'Anna', 'Rose', 'Peter', 'Peter', 'Anna']));
// console.log(getTuple([1, 10, 12, 2, 1, 10, 2, 2]));
// console.log(getTuple([true, false, true]));


// ----=====Type guard=====---- //

/**
 Задание 8:
 Напишите функцию isUser "защитник типа", которая принимает на вход параметр
 неизвестного типа и уточняет, что параметр является пользователем - объект с 2
 обязательными свойствами firstName и age.
 Использовать:
 - is (описание предиката)
 - in (проверка на наличие свойства)
 - as (отключение типизации для проверки)
 - interface (для описания типа User)
 const user: unknown = JSON.parse('{"firstName": "Alisa", "age": 15}');
 if (isUser(user)) {
    console.log(user.firstName, user.age);
  }
 */

interface User2 {
    firstName: string,
    age: number
}

const user: unknown = JSON.parse('{"firstName": "Alisa", "age": 15}');

const isUser = (obj: unknown): obj is User2 => {
    if (obj && typeof obj === 'object') {
        return 'firstName' in obj && 'age' in obj
    }
    return false
}
if (isUser(user)) {
    console.log(user.firstName, user.age);
}


// ----=====Интерфейсы и классы=====---- //

/**
 Задание 9 - Опишите интерфейс пользователя со следующими свойствами. Создайте подходящий объект.
 - firstName (строка) - имя
 - secondName (строка) - фамилия
 - middleName (строка, необязательное поле) - среднее имя
 */

interface User3 {
    firstName: string,
    secondName: string,
    middleName?: string
}

const user31: User3 = {
    firstName: "Lena",
    secondName: "Yagolovach"
}

/**
 Задание 10:
 Опишите интерфейс пользователя посложнее.
 Пользователь, как все сущности системы полученные из базы данных имеет мета-свойства базовой
 сущности BaseEntity:
 - id (строка) - идентификатор пользователя
 - addedAt (Дата) - время создания сущности
 - updatedAt (Дата) - время обновления сущности
 - addedBy (строка) - кто добавил
 - updatedBy (строка) - кто последний раз обновил
 Свойства интерфейса User:
 - roles (массив типа Role) - список ролей пользователя
 - firstName (строка) - имя
 - secondName (строка) - фамилия
 - middleName (строка, необязательное поле) - среднее имя
 - isAdmin (логическое, только для чтения) - является ли пользователь администратором
 Интферфейс Role также имеет все базовые свойства интерфейса BaseEntity и свои:
 - name (строка) - наименование роли
 */

interface BaseEntity {
    id: string,
    addedAt: Date,
    updatedAt: Date,
    addedBy: string,
    updatedBy: string
}

interface Role extends BaseEntity {
    name: string
}

interface User4 extends BaseEntity {
    roles: Role[],
    firstName: string,
    secondName: string,
    middleName?: string,
    isAdmin: boolean
}

const user41: User4 = {
    id: `id1234`,
    addedAt: new Date(),
    updatedAt: new Date(),
    addedBy: `Uncle`,
    updatedBy: `Uncle`,
    firstName: `Some`,
    secondName: `Guy`,
    isAdmin: false,
    roles: [
        {
            id: `id0001`,
            addedAt: new Date(),
            updatedAt: new Date(),
            addedBy: `Uncle`,
            updatedBy: `Uncle`,
            name: `Uncle`
        }
    ]
}


/**
 * Задание 11:
 Опишите следующие геометрические фигуры используя ООП:
 - Точка
 P = 0
 S = 0
 - Круг
 P = 2 * r * π
 S = π × r ^ 2
 - Квадрат
 P = 4 * a
 S = a ^ 2
 - Прямоугольник
 P = 2 * a + 2 * b
 S = a * b

 Все фигуры имеют координаты и возможность получить информацию о периметре и площади.
 Необходимо аннотировать все переменные, использовать имплементацию интерфейсов, наследование,
 переопределение методов.

 Компоненты:
 - Интерфейс Shape (фигура)
 - Класс Dot (точка)
 - Класс Circle (круг)
 - Класс Square (квадрат)
 - Класс Rectangle (прямоугольник)

 Методы:
 - calcArea()
 - calcPerimeter()

 Свойства:
 - x
 - y
 - r
 - a
 - b
 */

interface Shape {
    coordinateX: number,
    coordinateY: number,

    calcArea(): number,

    calcPerimeter(): number
}

class Figure implements Shape {
    constructor(public coordinateX: number, public coordinateY: number) {
    }

    calcArea(): number {
        return 0
    }

    calcPerimeter(): number {
        return 0
    }
}

class Dot extends Figure {
    constructor(coordinateX: number, coordinateY: number) {
        super(coordinateX, coordinateY);
    }
}

class Circle extends Figure {
    constructor(coordinateX: number, coordinateY: number, public radius: number) {
        super(coordinateX, coordinateY);
    }

    calcArea(): number {
        return Math.round(Math.PI * Math.pow(this.radius, 2));
    }

    calcPerimeter(): number {
        return Math.round(2 * this.radius * Math.PI);
    }
}

class Square extends Figure {
    constructor(coordinateX: number, coordinateY: number, public sideA: number) {
        super(coordinateX, coordinateY);
    }

    calcArea(): number {
        return Math.pow(this.sideA, 2);
    }

    calcPerimeter(): number {
        return 4 * this.sideA;
    }
}

class Rectangle extends Figure {
    constructor(coordinateX: number, coordinateY: number, public sideA: number, public sideB: number) {
        super(coordinateX, coordinateY);
    }

    calcArea(): number {
        return this.sideA * this.sideB;
    }

    calcPerimeter(): number {
        return 2 * this.sideA + 2 * this.sideB;
    }
}